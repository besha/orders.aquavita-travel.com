Orders = new Meteor.Collection('orders');
Meteor.subscribe('orders');

Meteor.autorun(function(){
    var id = Session.get('order_id'),
        create_at = Session.get('create_at');
    if(id && create_at){
        Meteor.subscribe('orderById', id, create_at);
    }
    
});

var reset = function(ctx, next){
	Session.set('order_id', undefined);
	Session.set('query', undefined);
	Session.set('path', ctx.path);
	Session.set('gpwebpay_order', undefined);
	Session.set('create_at', undefined);
	next();
};

var order = function(ctx){
	Session.set('__PAGE__', 'order');
	Session.set('order_id', ctx.params.id);
};

var list = function(ctx){
	Session.set('__PAGE__', 'list');
	Session.set('query', ctx.querystring);
};


var invoice = function(ctx, next){
	Session.set('__PAGE__', 'invoice');
	Session.set('order_id', ctx.params.id);
	Session.set('create_at', ctx.params.create_at);
};

var pay = function(ctx){
	Session.set('__PAGE__', 'pay');
	Session.set('order_id', ctx.params.id);
	Session.set('create_at', ctx.params.create_at);
	var params = _.reduce(ctx.querystring.split("&"), function(obj, param){
		var pair = param.split('='),
			key = decodeURIComponent(pair[0]),
			val = decodeURIComponent(pair[1]);
		obj[key] = val;
		if(key == 'ORDERNUMBER' || key == 'MERORDERNUM'){
			obj[key] = parseInt(val);
		}else{
			obj[key] = val;
		}
		return obj;
	}, {});
	Meteor.call('makeGPWebPayPayment', params, function(err, res){
		if(res.status){
			page('/invoice/' + ctx.params.id + '/' + ctx.params.create_at);
		}else{
			Session.set('error', params.RESULTTEXT);
			Session.set('back_url', '/invoice/' + ctx.params.id + '/' + ctx.params.create_at);
			page('/error');
		}
	});
}

var error = function(ctx){
	Session.set('__PAGE__', 'error');
}


page('/', function(){
	page('/list');
});

page('/list', reset, list);
page('/order', reset, order);
page('/order/:id', reset, order);
page('/invoice/:id/:create_at', reset, invoice);
page('/pay/:id/:create_at', reset, pay);
page('/error', reset, error);

page();