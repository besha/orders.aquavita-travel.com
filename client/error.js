Template.error.msg = function(){
	return Session.get('error');
};

Template.error.back_url = function(){
	return Session.get('back_url');
}