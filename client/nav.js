Template.nav_bar.events({
	'submit form[data-role="search"]': function(evt){
		evt.preventDefault();
		var $form = $(evt.currentTarget),
			query = $form.find('[name="query"]').val();
			
		page('/list?' + query);
		
	}
});