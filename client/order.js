var getOrderBySessionKey = function(){
	return Orders.findOne(Session.get('order_id'));
};

var calculateInvoiceTotalPrice = function(){
	var $form = $('form[data-role="invoice-form"]'),
		$prices = $form.find('[data-role="total-price"]'),
		$invoice_total = $form.find('[data-role="invoice-total-price"]'),
		invoice_total = _.reduce($prices, function(total, price){
			var $price = $(price),
				_total = parseInt($price.text()) || 0;
				return total + _total;
		}, 0);
		
		$invoice_total.text(invoice_total);
};

var enableSaveButton = function(evt){
	var $button = $('[data-role="save-order-form"]');
	$button.removeAttr('disabled');
}


Template.list.orders = function(){
	var query = Session.get('query'),
		sort = {sort: {create_at: 1, modify_at: 1}};
	if(query){
		var search = {$regex: '.*' + query + '.*', $options: 'i'};
		return Orders.find({
			$or: [
				{desc: search},
				{purchaser: search},
				{'items.name': search}
			]
		}, sort);
	}
	return Orders.find({}, sort);
}
Template.order.order = getOrderBySessionKey;

Template.order_form.events({
	'click [data-action="add-order-form-item"]': function(evt){
		var $target = $(evt.currentTarget),
			$parent = $target.parents('div.row');
		evt.preventDefault();
		$parent.before(Meteor.render(Template.order_form_item));
	},
	'keyup input, keyup textarea': enableSaveButton,
	'submit form[data-role="invoice-form"]': function(evt){
		evt.preventDefault();
		var $form = $(evt.currentTarget),
			order_id = $form.data('id'),
			desc = $form.find('[name="desc"]').val(),
			purchaser = $form.find('[name="purchaser"]').val(),
			items = _.map($form.find('[data-role="invoice-item"]'), function(item){
				var $item = $(item);
				return {
					amount: parseInt($item.find('[name="amount"]').val()),
					amount_type: $item.find('[name="amount_type"]').val(),
					name: $item.find('[name="name"]').val(),
					price: parseInt($item.find('[name="price"]').val()),
					total_price: parseInt($item.find('[data-role="total-price"]').text())
				}
			}),
			invoice_total = parseInt($form.find('[data-role="invoice-total-price"]').text());
		
		if(order_id){
			Orders.update(order_id, {$set: {
				desc: desc,
				purchaser: purchaser,
				items: items,
				invoice_total: invoice_total,
				modify_at: (new Date).valueOf()
			}});
		}else{
			order_id = Orders.insert({
				desc: desc,
				purchaser: purchaser,
				items: items,
				invoice_total: invoice_total,
				create_at: (new Date).valueOf()
			});
		}
		
		page('/order/' + order_id);
	}
});


Template.order_form_item.events({
	'keyup [data-action="calculate-total-price"]': function(evt){
		var $target = $(evt.currentTarget),
			$item = $target.parents('[data-role="invoice-item"]'),
			$total = $item.find('[data-role="total-price"]'),
			amount = parseInt($item.find('[name="amount"]').val()),
			price = parseInt($item.find('[name="price"]').val());
		
		if(amount && price){
			$total.text(amount*price);
		}else{
			$total.text('');
		}
		
		calculateInvoiceTotalPrice();
	},
	'keyup input, keyup textarea': enableSaveButton
});


Template.invoice.order = getOrderBySessionKey;

Template.payment.isPaid = function(){
	var order = getOrderBySessionKey();
	return order.payment_at;
}

Template.payment.events({
    'click [data-action="goto-gpwebpay"]': function(evt){
        var id = Session.get('order_id'),
            $target = $(evt.currentTarget);
        $target.button('loading');
        Meteor.call('getGPWebPayGetURI', id, function(err, res){
            if(res){
                window.location.href = res;
            }
        });
    }
});