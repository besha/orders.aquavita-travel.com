Handlebars.registerHelper('renderPage', function(name, options) {
    if (! _.isString(name)){
        name = Session.get('__PAGE__');
    }
    if (Template[name]){
        return new Handlebars.SafeString(Template[name]());
    }else{
        return new Handlebars.SafeString("<h1>404</h1>");
    }
});

Handlebars.registerHelper("isEqual", function(str1, str2){
      return str1 == str2;
});

Handlebars.registerHelper("isActivePage", function(page){
      return Session.equals('path', page);
});

// USAGE: isPageStarts "/invoice" "/pay" ...
Handlebars.registerHelper("isPageStarts", function(){
    var current_page = Session.get('path') || '';
	for(var i=0; i<arguments.length; i++){
		var patt = new RegExp("^" + arguments[i]);
		if(current_page.match(patt)){
			return true;
		}
	}
	return false;
	
});


Handlebars.registerHelper("date", function(date){
      var str = '';
      if(date){
          var date = new Date(date);
        return date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
      }
      return '';
});