PERM = {
    "god": "Может все",
    "orders": "Создавать заявки"
};


// Вовращает callback если есть права
checkUserPermission = function(userId, permission, callback, fail_callback){
    var user = Meteor.users.findOne({_id: userId});
    if(user && ( _.contains(user.permissions, 'god') || _.contains(user.permissions, permission) ) ){
        if(!_.isFunction(callback)){
            return true;
        }
        return callback();
    }
    return fail_callback && fail_callback() || false;
}