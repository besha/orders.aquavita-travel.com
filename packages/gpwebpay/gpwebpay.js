var MERCHANTNUMBER = "53277836",
    OPERATION = "CREATE_ORDER",
    CURRENCY = "978",
    DEPOSITFLAG = "1",
    GPWEBPAY_URL = "https://3dsecure.gpwebpay.com/csob/order.do";
    
GPWebPayOrder = function(order) {
    _.extend(this, order);
};
_.extend(GPWebPayOrder.prototype, {
    generateGPWebPayId: function() {
        var gpwebpay_id = (new Date).valueOf();
        Orders.update(this._id, {$push: {gpwebpay_ids: gpwebpay_id}});
        return gpwebpay_id;
    },
    getInnerNumericId: function() {
        return this.create_at;
    },
    getPaymentConfirmationUrl: function() {
        return Meteor.absoluteUrl("pay/" + this._id + '/' + this.create_at);
    },
    sign: function(data){
        var values = _.chain(data)
                        .map(function(param){
                            return param.value;
                        })
                        .value()
                        .join("|"),
            encodedData = encodeURIComponent(values);
        
        return HTTP.get('http://sign.aquavita-travel.com/sign.php?data=' + encodedData).content;
    },
    getPaymentFormData: function(){
        var data = [
                {name: "MERCHANTNUMBER", value: MERCHANTNUMBER},
                {name: "OPERATION", value: OPERATION},
                {name: "ORDERNUMBER", value: this.generateGPWebPayId()},
                {name: "AMOUNT", value: this.invoice_total * 100},
                {name: "CURRENCY", value: CURRENCY},
                {name: "DEPOSITFLAG", value: DEPOSITFLAG},
                {name: "MERORDERNUM", value: this.getInnerNumericId()},
                {name: "URL", value: this.getPaymentConfirmationUrl()},
                {name: "DESCRIPTION", value: this.desc}
            ],
            digest = this.sign(data);
        
        data.push({
            name: "DIGEST", value: digest
        });
        
        return data;
    },
    getGPWebPayGetURI: function(){
        var params = _.chain(this.getPaymentFormData())
                        .map(function(param){
                            return param.name + "=" + encodeURIComponent(param.value);
                        })
                        .value()
                        .join("&");
                        
        return GPWEBPAY_URL + '?' + params;
    },
	makePayment: function(params){
		var upd = {$push: {gpwebpay_log: params}},
			status = false;
		if(params.PRCODE == "0"){
			upd["$set"] = {payment_at: (new Date).valueOf()};
			status = true;
		}
		Orders.update(this._id, upd);
		return status;
	}
});