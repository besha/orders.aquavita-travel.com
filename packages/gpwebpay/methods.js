Meteor.methods({
    
    getGPWebPayGetURI: function(order_id){
        var order = Orders.findOne({_id: order_id}, {transform: function(order){
                return new GPWebPayOrder(order);
            }}),
            uri = order.getGPWebPayGetURI();
            
        return uri;
    },
	makeGPWebPayPayment: function(params){
		var order = Orders.findOne({
				create_at: params.MERORDERNUM,
				gpwebpay_ids: params.ORDERNUMBER
			}, {transform: function(order){
	                return new GPWebPayOrder(order);
	            }}),
			status;
		if(order){status = order.makePayment(params);}
		return {
			status: status,
			text: params.RESULTTEXT
		};
	}

});