Package.describe({
  summary: "GPWebPay.cz"
});

Package.on_use(function (api) {
    api.use(['underscore'], 'server');
    api.add_files(['gpwebpay.js', 'methods.js'], 'server');
});