// Страны с городами, для поселения гостей
Orders = new Meteor.Collection('orders');
Meteor.publish("orders", function () {
    return checkUserPermission(this.userId, 'orders', function(){
        return Orders.find();
    });
});

Meteor.publish("orderById", function (id, create_at) {
    var create_at = parseInt(create_at);
    return Orders.find({_id: id, create_at: create_at});
});

Orders.allow({
    insert: function(userId, doc){
        return checkUserPermission(userId, 'orders');
    },
    update: function(userId, doc, fieldNames, modifier){
        return checkUserPermission(userId, 'orders');
    },
    remove: function(userId, doc){
        return false;
    }
})